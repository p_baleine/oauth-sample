var qs = require('querystring'),
    url = require('url'),    
    http = require('http'),
    Promise = require('es6-promise').Promise;

// `urlStr`にPOSTリクエストを投げる
exports.post = function(urlStr, params) {
  return new Promise(function(resolve, reject) {
    var parsed = url.parse(urlStr),
        data = qs.stringify(params),
        options = {
          hostname: parsed.hostname,
          method: 'POST',
          path: parsed.path,
          port: parsed.port,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': data.length
          }
        },
        request = http.request(options, function(res) {
          var data = '';

          res.setEncoding('utf8');
          res.on('data', function(chunk) {
            data += chunk;
          });
          res.on('end', function() {
            resolve(data);
          });
        }).on('error', function(e) {
          reject(e);
        });

    request.write(data);
    request.end();
  });
}

// `url`にGETリクエストを投げる
exports.get = function(url, params) {
  return new Promise(function(resolve, reject) {
    if (params) { url += '?' + qs.stringify(params); }

    http.get(url, function(res) {
      var data = '';

      res.setEncoding('utf8');
      res.on('data', function(chunk) {
        data += chunk;
      });
      res.on('end', function() {
        resolve(data);
      });
    }).on('error', function(e) {
      reject(e);
    });
  });
}
