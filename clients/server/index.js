var app = require('./lib/app'),
    config = require('config');

app.listen(config.port, function() {
  console.log('server-side client started on port %s.', config.port);
});
