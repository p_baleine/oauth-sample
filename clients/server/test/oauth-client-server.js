var expect = require('chai').expect,
    request = require('supertest'),
    config = require('config'),
    url = require('url'),
    qs = require('querystring'),

    app = require('../lib/app');

describe('oauth-client-server', function() {

  describe('OAuth認可が必要なリソースにリソースオーナーがアクセスした場合', function() {

    var authorizationUrl = url.parse(config.oauthSettings.authorizationUrl);

    function accessToResourceReuiringAuthorization(cb) {
      request(app).get('/secret').end(cb);
    }

    function responseQuery(response) {
      return qs.parse(url.parse(response.headers.location).query);
    }

    it('サーバ(プロバイダ)の認可ページへリダイレクトすること', function(done) {
      accessToResourceReuiringAuthorization(function(err, res) {
        if (err) { return done(err); }
        expect(res.statusCode).to.equal(302);          
        expect(url.parse(res.headers.location).hostname).to.equal(authorizationUrl.hostname);
        expect(url.parse(res.headers.location).port).to.equal(authorizationUrl.port);
        expect(url.parse(res.headers.location).pathname).to.equal(authorizationUrl.pathname);
        done();
      });
    });

    it('`client_id`をリダイレクトURLのパラメタに付与すること', function(done) {
      accessToResourceReuiringAuthorization(function(err, res) {
        if (err) { return done(err); }
        expect(url.parse(res.headers.location).query).to.exists;
        expect(Number(responseQuery(res).client_id)).to.equal(config.oauthSettings.clientId);
        done();
      });
    });

    it('`response_type`に`code`を指定してリダイレクトURLのパラメタに付与すること', function(done) {
      accessToResourceReuiringAuthorization(function(err, res) {
        if (err) { return done(err); }
        expect(responseQuery(res).response_type).to.equal('code');
        done();
      });
    });

    it('`redirect_uri`をリダイレクトURLのパラメタに付与すること', function(done) {
      accessToResourceReuiringAuthorization(function(err, res) {
        if (err) { return done(err); }
        expect(responseQuery(res).redirect_uri).to.equal(config.oauthSettings.redirectUrl);
        done();
      });
    });

    describe('リソースオーナー(ユーザ)が認可ページにて認可した場合', function() {
      it('認可コードでアクセストークンを取得すること');
      it('リソースのページへリダイレクトすること');

      describe('リソースのページにて', function() {
        it('アクセストークンを用いてAPI問い合わせすること');
        it('API問い合わせの結果を描画すること');
      });

    });

  });


});
