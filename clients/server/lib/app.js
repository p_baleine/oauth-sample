var express = require('express'),
    path = require('path'),
    config = require('config'),
    qs = require('querystring'),
    util = require('../../../share/util'),

    app = module.exports = express(),
    oauthSettings = config.oauthSettings;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.cookieParser('your secret here'));
app.use(express.session());

app.get('/', function(req, res) {
  res.render('index');
});

// OAuth認可が必要なルート
app.get('/todos', function(req, res) {
  // サーバ(プロバイダ)の認可ページへリダイレクト
  var url = oauthSettings.authorizationUrl + '?' + qs.stringify({
    client_id: oauthSettings.clientId,
    response_type: 'code',
    redirect_uri: oauthSettings.redirectUrl,
    scope: 'any'
  });

  res.redirect(url);
});

// リソースオーナー(ユーザ)が認可したときのコールバック
app.get('/oauth_callback', function(req, res, next) {
  // 認可コードでアクセストークンをリクエスト
  var code = req.query.code,
      params = {
        client_id: oauthSettings.clientId,
        client_secret: oauthSettings.clientSecret,
        redirect_uri: oauthSettings.redirectUrl,
        code: code,
        grant_type: 'authorization_code'
      };

  util.post(oauthSettings.accessTokenUrl, params)
    .then(function(body) {
      // 応答のbodyからアクセストークンを取得してセッションに保持
      req.session.access_token = JSON.parse(body).access_token;
      // 結果ページへリダイレクト
      res.redirect('/result');
    })
    .catch(next);
});

// 結果ページ
app.get('/result', function(req, res, next) {
  var accessToken = req.session.access_token;

  // アクセストークンでリソースにアクセスして結果表示
  util.get({
    hostname: 'localhost',
    port: 3000,
    path: '/api/todos?' + qs.stringify({ access_token: accessToken }),
    method: 'GET',
    headers: {
      accept: 'application/json'
    }
  })
    .then(function(body) {
      res.render('result', { message: body });
    })
    .catch(next);
});
