var express = require('express'),
    Resource = require('express-resource'),
    path = require('path'),
    _ = require('underscore'),
    user = require('./routes/user'),
    todo = require('./routes/todo'),
    session = require('./routes/session'),
    User = require('./models/user'),
    oauth = require('./oauth'),

    api = module.exports.api = express(),
    app = module.exports = express();

// settings

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// middlewares

app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser('your secret here'));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, '../public')));

// routes

app.all('/todos', loadUser);

app.get('/', function(req, res) {
  res.redirect('/todos');
});

app.resource('sessions', session);
app.resource('users', user);
app.resource('todos', todo);
app.use('/oauth', oauth);

// api

api.all('/todos', loadUser);

api.resource('todos', todo);

app.use('/api', api);

// ユーザをフェッチして`request.user`に格納する
function loadUser(req, res, next) {
  var userId;

  if (req.query.access_token) {
    if (!oauth.ACCESS_TOKEN['TOKEN_' + req.query.access_token]) {
      res.statusCode = 403;
      return res.send({ error: 'invalid token' });
    }

    userId = oauth.ACCESS_TOKEN['TOKEN_' + req.query.access_token].user_id;
  } else {
    // 未ログイン又はログインしているユーザのリソースではない場合
    // ログインページへリダイレクト
    if (!_.has(req.session, 'user_id')) { return res.redirect('/sessions/new'); }

    userId = req.session.user_id;
  }

  // ユーザをフェッチして終わり
  new User({ id: userId }).fetch()
    .then(function(user) {
      req.user = res.locals.user = user;
      next();
    })
    .catch(next);
}
