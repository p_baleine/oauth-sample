var express = require('express'),
    path = require('path'),
    qs = require('querystring'),
    _ = require('underscore'),
    config = require('config'),
    uuid = require('node-uuid'),

    app = module.exports = express();

// TODO error handler

// TODO storing clients in redis
var CLIENTS = {
  '1': {
    secret: '1secret'
  }
};

var AUTH_INFO = {};

module.exports.ACCESS_TOKEN = {};

// TODO セッションもredisかな

// settings

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.get('/authorize', function(req, res, next) {
  // ログインして戻ってきた場合、認可ページを描画
  if (req.session.auth_info) { return res.render('authorize'); }

  var clientId = req.query.client_id,
      callbackUrl = req.query.redirect_uri,
      responseType = req.query.response_type,
      scope = req.query.scope;

  if (!_.has(CLIENTS, clientId)) {
    // 無効なクライアントID
    redirectToCallbackUrl(res, callbackUrl, { error: 'invalid_client' });
  } else if (!validScope(scope)) {
    // 無効なスコープ
    redirectToCallbackUrl(res, callbackUrl, { error: 'invalid_scope' });
  } else if (!_.include(['code'], responseType)) {
    // サポート外のレスポンスタイプ
    redirectToCallbackUrl(res, callbackUrl, { error: 'unsupported_response_type' });
  } else {
    req.session.auth_info = {
      client_id: clientId,
      callback_url: callbackUrl,
      response_type: responseType,
      scope: scope
    };

    if (_.has(req.session, 'user_id')) {
      res.render('authorize');
    } else {
      res.render('login');
    }
  }
});

app.post('/authorize', function(req, res, next) {
  var authInfo = req.session.auth_info,
      authCode = uuid.v4(),
      key = 'AUTH_INFO_' + authCode;

  authInfo.user_id = req.session.user_id;
  AUTH_INFO[key] = authInfo;

  redirectToCallbackUrl(res, authInfo.callback_url, { code: authCode });
});

app.post('/access_token', function(req, res, next) {
  var clientId = req.body.client_id,
      clientSecret = req.body.client_secret,
      callbackUrl = req.body.redirect_uri,
      grantType = req.body.grant_type;

  if (!CLIENTS[clientId] || CLIENTS[clientId].secret !== clientSecret) {
    return res.send({ error: 'invalid client' });
  }

  if (grantType === 'authorization_code') {
    var authCode = req.body.code,
        key = 'AUTH_INFO_' + authCode,
        authInfo = AUTH_INFO[key];

    if (!authInfo || callbackUrl !== authInfo.callback_url) {
      return res.send({ error: 'invalid grant' });
    }

    // delete AUTH_INFO[key];

    var accessToken = uuid.v4(),
        refreshToken = uuid.v4();

    authInfo.accessToken = accessToken;

    var tokenKey = 'TOKEN_' + accessToken;

    module.exports.ACCESS_TOKEN[tokenKey] = authInfo;

    res.send({
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: 900
    });
  }
  // TODO refreshToken
});

// TODO todosに繋げる
app.get('/secret', function(req, res, next) {
  var authInfo = module.exports.ACCESS_TOKEN['TOKEN_' + req.query.access_token];

  if (!authInfo) {
    res.statusCode = 403;
    return res.send({ error: 'invalid token' });
  }

  res.send('hello secret!!!');
});

function redirectToCallbackUrl(res, url, params) {
  // TODO params
  if (params) { url += '?' + qs.stringify(params); }
  res.redirect(url);
}

function validScope(scope) {
  return _.include(config.validScopes, scope);
}
