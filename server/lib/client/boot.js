var $ = require('jquery'),
    Backbone = require('backbone'),
    BackboneStickit = require('BackboneStickit'),
    App = require('./app-router');

Backbone.$ = $;

$(function() {
  var app = new App();

  Backbone.history.start();
});
