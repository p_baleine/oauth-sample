var Backbone = require('backbone'),
    AppView = require('./app-view');

var AppRouter = module.exports = Backbone.Router.extend({

  routes: {
    '': 'index'
  },

  initialize: function() {
    this.view = new AppView({ el: 'body' });
  }

});
