var Backbone = require('backbone'),
    TodoItemView = require('./todo-item-view');

var TodoListView = module.exports = Backbone.View.extend({

  initialize: function() {
    this.collection.on('add', this.renderOne, this);
  },

  // `.todo`クラスを持つ要素を`TodoItemView`にヒモづける
  setChildElements: function() {
    this.$('.todo').each(Backbone.$.proxy(function(idx, elt) {
      var todoId = Number(Backbone.$(elt).data('todo-id')),
          todo = this.collection.get(todoId),
          view = new TodoItemView({ model: todo });

      view.setElement(elt);
      view.stickit();
    }, this));

    return this;
  },

  renderOne: function(model) {
    this.$el.append(new TodoItemView({ model: model }).render().el);

    return this;
  }

});
