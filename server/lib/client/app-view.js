var Backbone = require('backbone'),
    TodoListView = require('./todo-list-view'),
    Todos = require('./todo-collection');

var AppView = module.exports = Backbone.View.extend({

  events: {
    'submit form': 'create'
  },

  initialize: function() {
    this.todos = new Todos(window.todos);
    new TodoListView({ el: '.todos', collection: this.todos })
      .setChildElements();
  },

  create: function(e) {
    e.preventDefault();

    var title = this.$('[name="todo[title]"]').val();

    this.todos.create({ title: title });
  }

});
