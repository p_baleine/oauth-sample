var _ = require('underscore'),
    Backbone = require('backbone');

var TodoItemView = module.exports = Backbone.View.extend({

  tagName: 'li',

  className: 'todo',

  events: {
    'dblclick .view': 'toggleEditable',
    'keydown .edit': 'update',
    'click .destroy': 'destroy'
  },

  bindings: {
    '.edit': 'title',
    '.view': 'title',
    '.done': 'done'
  },

  initialize: function() {
    this.listenTo(this.model, 'error', this.onError);
    this.listenTo(this.model, 'destroy', this.remove);
    this.listenTo(this.model, 'change:done', _.bind(function() { this.model.save(); }, this));
  },

  render: function() {
    this.$el.html(this.template(this.model.toJSON()));
    this.stickit();

    return this;
  },

  toggleEditable: function() {
    this.$el.toggleClass('editable');
  },

  update: function(e) {
    if ((e.keyCode && e.keyCode !== 13) || (e.which && e.which !== 13)) { return; }
    e.preventDefault();
    this.model.save();
    this.toggleEditable();
  },

  destroy: function(e) {
    e.preventDefault();
    this.model.destroy();
  },

  onError: function() {
    alert('error');
  },

  template: require('../views/todo/item.jade')
});

