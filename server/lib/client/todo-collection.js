var Backbone = require('backbone'),
    Todo = require('./todo-model');

var Todos = module.exports = Backbone.Collection.extend({

  model: Todo

});
