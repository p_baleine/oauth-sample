var User = require('../models/user');

exports.new = function(req, res) {
  res.render('session/new');
};

exports.create = function(req, res) {
  var user = req.body.user,
      fromAuth = req.body.fromAuth && req.body.fromAuth === '1';

  User.authenticate(user.email, user.password).then(function(user) {
    req.session.user_id = user.id;

    if (fromAuth) {
      // OAuth認可の場合、認可ページをレンダー
      res.redirect('/oauth/authorize');
    } else {
      res.redirect('/todos');
    }
  }, function(e) {
    req.session.user_id = null;
    res.statusCode = 404;
    // TODO flash
    res.render('session/new', { error: e.message });
  });
};

exports.destroy = function(req, res) {
  delete req.session.user_id;
  res.redirect('/');
};
