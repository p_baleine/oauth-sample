var _ = require('underscore'),
    Todo = require('../models/todo');

exports.index = function(req, res, next) {
  req.user.todos().fetch()
    .then(function(todos) {
      res.format({
        'text/html': function(){
          res.render('todo/index', { todos: todos.toJSON() });
        },
  
        'application/json': function(){
          res.send(todos.toJSON());
        }
      });
    })
    .catch(next);
};

exports.new = function(req, res) {
  res.render('todo/new');
};

exports.create = function(req, res, next) {
  var userId = req.user.id;

  new Todo(_.extend({ user_id: userId }, req.body)).save()
    .then(function(todo) {
      res.send(todo);
    })
    .catch(next);
};

exports.update = function(req, res, next) {
  var data = req.body,
      userId = req.user.id;

  new Todo({ id: req.params.todo }).fetch({ require: true })
    .then(function(todo) {
      if (todo.user_id !== userId) {
        res.statusCode = 404;
        throw new Error('not founr');
      }

      return todo.save(data);
    })
    .then(function(todo) {
      res.send(todo.toJSON());
    })
    .catch(next);
};

exports.destroy = function(req, res, next) {
  var userId = req.user.id;

  new Todo({ id: req.params.todo }).fetch()
    .then(function(todo) {
      if (todo.user_id !== userId) {
        res.statusCode = 404;
        throw new Error('not founr');
      }

      return todo.destroy();
    })
    .then(function() {
      res.send({});
    })
    .catch(next);
};
