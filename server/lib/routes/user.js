var _ = require('underscore'),
    User = require('../models/user');

exports.new = function(req, res) {
  res.render('user/new');
};

exports.create = function(req, res, next) {
  var confirm = req.body.user.confirm,
      user = new User(_.omit(req.body.user, 'confirm'));

  if (user.get('password') !== confirm) {
    // TODO redirect to `user/new` with flash messages.
    return next(new Error('invalid pass'));
  }

  user.save()
    .then(function(user) {
      req.session.user_id = user.id;
      res.redirect('/todos');
    })
    .catch(next);
};
