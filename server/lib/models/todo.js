var BaseModel = require('./base');

var Todo = module.exports = BaseModel.extend({

  tableName: 'todos',

  hasTimestamps: true

});
