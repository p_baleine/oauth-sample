var Promise = require('bluebird'),
    bcrypt = Promise.promisifyAll(require('bcrypt')),
    BaseModel = require('./base'),
    Todo = require('./todo');

// ユーザモデル
var User = module.exports = BaseModel.extend({

  HASH_COUNT: 10,

  tableName: 'users',

  hasTimestamps: true,

  // todoへのリレーション
  todos: function() {
    return this.hasMany(Todo);
  },

  initialize: function() {
    BaseModel.prototype.initialize.apply(this, arguments);
    this.on('saving', this.hashPassword, this);
  },

  // saltを生成し、これを用いてハッシュしたパスワードをセットする
  hashPassword: function() {
    var _this = this;

    return bcrypt.genSaltAsync(_this.HASH_COUNT)
      .then(function(salt) {
        _this.set('salt', salt);
        return bcrypt.hashAsync(_this.get('password'), salt);
      })
      .then(function(hash) {
        return _this.set('password', hash);
      });
  }

}, {

  // `email`と`password`でユーザ認証する
  authenticate: function(email, password) {
    return new this({ email: email }).fetch({ require: true })
      .then(function(user) {
        return [bcrypt.hashAsync(password, user.get('salt')), user];
      })
      .spread(function(hash, user) {
        if (hash === user.get('password')) { return user; }
        throw new Error('パスワード又はメールアドレスが間違っています。');
      });
  }

});
