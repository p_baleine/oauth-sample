var Bookshelf = require('bookshelf'),
    config = require('config'),

    TodoBookShelf = Bookshelf.TodoBookShelf = Bookshelf.initialize(config.database);

var BaseModel = module.exports = TodoBookShelf.Model.extend({
}, {

  findAll: function() {
    var coll = TodoBookShelf.Collection.forge([], { model: this });

    return coll.fetch.apply(coll, arguments);
  }

});
