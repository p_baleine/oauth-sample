var app = require('./lib/app'),
    config = require('config');

app.listen(config.port, function() {
  console.log('server started on port %d', config.port);
});

