var config = require('config');

module.exports = {
  directory: config.migrationsDirectory,
  database: config.database
};
