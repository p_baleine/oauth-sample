
exports.up = function(knex, Promise) {
  return knex.schema.createTable('todos', function(t) {
    t.increments('id').primary();
    t.integer('user_id').references('id').inTable('users');
    t.string('title').notNullable();
    t.boolean('done').notNullable().defaultTo(false);
    t.timestamps();
  });
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTable('todos');  
};
