# oauth-sample/server

OAuth2認証におけるサーバ(サービスプロバイダ)のサンプル

提供するWebサービスとしては単純なTODOリスト

## Requirement

* sass

## サーバの起動

```bash
$ git clone git clone https://p_baleine@bitbucket.org/p_baleine/oauth-sample.git
$ cd server
$ npm start
```

