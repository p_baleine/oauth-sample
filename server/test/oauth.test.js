var expect = require('chai').expect,
    express = require('express'),
    qs = require('querystring'),
    url = require('url'),
    request = require('supertest'),
    _ = require('underscore'),
    oauth = require('../lib/oauth');

describe('oauth', function() {

  beforeEach(function() {
    this.app = express();
    this.app.use(express.cookieParser('your secret here'));
    this.app.use(express.session());
  });

  var defaultParams = {
    client_id: 1,
    redirect_uri: 'http://hoge.com',
    response_type: 'code',
    scope: 'any'
  };

  // クエリパラメタを構築する
  function querystring(params) {
    return qs.stringify(_.extend({}, defaultParams, params));
  }

  describe('GET /authorize', function() {
    describe('無効な認可情報を送信している場合', function() {

      beforeEach(function() {
        this.app.use(oauth);
      });

      describe('無効なクライアントIDでリクエストしている場合', function() {
        it('コールバックURLにエラーで応答すること', function(done) {
          request(this.app)
            .get('/authorize?' + querystring({ client_id: 99 }))
            .expect(302)
            .end(function(err, res) {
              if (err) { return done(err); }
              expect(url.parse(res.headers.location).hostname)
                .to.equal(url.parse(defaultParams.redirect_uri).hostname);
              done();
            });
        });
      });

      describe('無効なスコープでリクエストしている場合', function() {
        it('コールバックURLにエラーで応答すること', function(done) {
          request(this.app)
            .get('/authorize?' + querystring({ scope: 'hoge' }))
            .expect(302)
            .end(function(err, res) {
              if (err) { return done(err); }
              expect(url.parse(res.headers.location).hostname)
                .to.equal(url.parse(defaultParams.redirect_uri).hostname);
              done();
            });
        });
      });

      describe('サポート外のレスポンスタイプでリクエストしている場合', function() {
        it('コールバックURLにエラーで応答すること', function(done) {
          request(this.app)
            .get('/authorize?' + querystring({ response_type: 'hoge' }))
            .expect(302)
            .end(function(err, res) {
              if (err) { return done(err); }
              expect(url.parse(res.headers.location).hostname)
                .to.equal(url.parse(defaultParams.redirect_uri).hostname);
              done();
            });
        });
      });
    });

    describe('有効な認可情報を送信している場合', function() {
      describe('ログイン済ユーザの場合', function() {

        beforeEach(function() {
          // セッションをモック
          this.app.use(function(req, res, next) {
            req.session.user_id = 2;
            next();
          });
          this.app.use(oauth);
        });

        it('認可ページを描画すること', function(done) {
          request(this.app)
            .get('/authorize?' + querystring())
            .expect(200)
            .end(function(err, res) {
              if (err) { return done(err); }
              expect(res.text).to.match(/form/);
              expect(res.text).to.match(/action=['"]\/oauth\/authorize['"]/);
              done();
            });
        });
      });

      describe('未ログインユーザの場合', function() {

        beforeEach(function() {
          this.app.use(oauth);
        });

        it('ログインページを描画すること', function(done) {
          request(this.app)
            .get('/authorize?' + querystring())
            .expect(200)
            .end(function(err, res) {
              if (err) { return done(err); }
              expect(res.text).to.match(/form/);
              expect(res.text).to.match(/action=['"]\/sessions['"]/);
              done();
            });
        });
      });
    });
  });

  describe('POST /authorize', function() {
    describe('ユーザが認可した場合', function() {

      beforeEach(function() {
        this.app.use(function(req, res, next) {
          req.session.user_id = 2;
          req.session.auth_info = defaultParams;
          req.session.auth_info.callback_url = defaultParams.redirect_uri;
          next();
        });
        this.app.use(oauth);
      });

      it('コールバックURLへリダイレクトすること', function(done) {
        request(this.app)
          .post('/authorize')
          .end(function(err, res) {
            if (err) { return done(err); }
            expect(url.parse(res.headers.location).hostname)
              .to.equal(url.parse(defaultParams.redirect_uri).hostname);
            done();
          });
      });

      it('リダイレクト時に認可コードをURLに付与すること', function(done) {
        request(this.app)
          .post('/authorize')
          .end(function(err, res) {
            if (err) { return done(err); }
            expect(qs.parse(url.parse(res.headers.location).query).code).to.exist;
            done();
          });
      });
    });

    describe('ユーザが認可を拒否した場合', function() {
      it('コールバックURLへリダイレクトすること');
    });
  });

  function authorize(done) {
    this.app.use(express.logger('dev'));
    this.app.use(express.bodyParser());
    this.app.use(function(req, res, next) {
      req.session.user_id = 2;
      req.session.auth_info = defaultParams;
      req.session.auth_info.callback_url = defaultParams.redirect_uri;
      next();
    });
    this.app.use(oauth);
    
    request(this.app).post('/authorize').end(function(err, res) {
      this.code = qs.parse(url.parse(res.headers.location).query).code;
      done();
    }.bind(this));
  }

  describe('/access_token', function() {
    beforeEach(function(done) {
      authorize.call(this, done);
    });

    describe('有効なclient_idとclient_secretを送信した場合', function() {
      describe('grant_typeに`authorization_code`を指定した場合', function() {
        describe('redirect_uriに`/authorize`時と同じURLを送信した場合', function() {
          it('access_tokenを返却すること', function(done) {
            request(this.app)
              .post('/access_token')
              .send({ client_id: 1, client_secret: '1secret',
                      redirect_uri: 'http://hoge.com', grant_type: 'authorization_code',
                      code: this.code })
              .expect(200)
              .end(function(err, res) {
                if (err) { return done(err); }
                expect(res.body.access_token).to.exist;
                done();
              });
          });
        });

        describe('redirect_uriに`/authorize`時と異なるURLを送信した場合', function() {
          it('エラーを返却すること');
        });
      });

      describe('grant_typeに`refresh_token`を指定した場合', function() {
        it('access_tokenを再発行して返却すること');
      });
    });

    describe('無効なclient_idを送信した場合', function() {
      it('エラーを返却すること');
    });

    describe('無効なclient_secretを送信した場合', function() {
      it('エラーを返却すること');
    });
  });

  function getAccessToken(done) {
    request(this.app)
      .post('/access_token')
      .send({ client_id: 1, client_secret: '1secret',
              redirect_uri: 'http://hoge.com', grant_type: 'authorization_code',
              code: this.code })
      .end(function(err, res) {
        this.accessToken = res.body.access_token;
        done();
      }.bind(this));
  }

  describe('OAuth認可が必要なリソースへのアクセス', function() {

    beforeEach(function(done) {
      var _this = this;

      authorize.call(_this, function() {
        getAccessToken.call(_this, done);
      });
    });

    describe('有効なアクセストークンを送信した場合', function() {
      it('リソースを返却すること', function(done) {
        request(this.app)
          .get('/secret?' + qs.stringify({ access_token: this.accessToken }))
          .expect(200)
          .end(function(err, res) {
            if (err) { return done(err); }
            done();
          });
      });
    });

    describe('無効なアクセストークンを送信した場合', function() {
      it('エラーを返却すること');
    });
  });
});
